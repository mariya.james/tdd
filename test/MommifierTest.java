import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;

public class MommifierTest {
    @Test
    public void shouldReturnEmptyStringWhenInputEmptyString() {
        Mommifier mommifier = new Mommifier();
        String output = mommifier.mommify("");
        assertEquals("", output);
    }

    @Test
    public void shouldReturnNullWhenInputNull() {
        Mommifier mommifier = new Mommifier();
        String output = mommifier.mommify(null);
        assertNull(output);
    }

    @Test
    public void shouldMommifyAVowel() {
        Mommifier mommifier = new Mommifier();
        String output = mommifier.mommify("a");
        assertEquals("mommy",output);
    }

    @Test
    public void shouldNotMommifyAConsonant() {
        Mommifier mommifier = new Mommifier();
        String output = mommifier.mommify("b");
        assertEquals("b",output);
    }

    @Test
    public void shouldReturnBcdWhenInputBcd() {
        Mommifier mommifier = new Mommifier();
        String output = mommifier.mommify("bcd");
        assertEquals("bcd",output);
    }

    @Test
    public void shouldReturnHardWhenInputHard() {
        Mommifier mommifier = new Mommifier();
        String output = mommifier.mommify("hard");
        assertEquals("hard",output);
    }
}
